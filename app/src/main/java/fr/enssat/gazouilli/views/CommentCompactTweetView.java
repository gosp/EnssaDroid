package fr.enssat.gazouilli.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.twitter.sdk.android.core.models.Tweet;
import com.twitter.sdk.android.tweetui.CompactTweetView;

import fr.enssat.gazouilli.R;

/**
 * Custom CompactTweetView extended with comments and more button
 */
public class CommentCompactTweetView extends CompactTweetView {

    // view references
    public LinearLayout tw__comment;
    public LinearLayout tw_comment1;
    public LinearLayout tw_comment2;
    public LinearLayout tw_comment3;
    public TextView tw_comment1_pseudo;
    public TextView tw_comment2_pseudo;
    public TextView tw_comment3_pseudo;
    public TextView tw_comment1_message;
    public TextView tw_comment2_message;
    public TextView tw_comment3_message;
    public Button tw_details;

    public OnMoreButtonClickListener moreButtonClickListener = null;

    //-- Constructors

    public CommentCompactTweetView(Context context, Tweet tweet) {
        super(context, tweet);
        findAdditionnalViews();
    }

    public CommentCompactTweetView(Context context, Tweet tweet, int styleResId) {
        super(context, tweet, styleResId);
        findAdditionnalViews();
    }

    public CommentCompactTweetView(Context context, AttributeSet attrs) {
        super(context, attrs);
        findAdditionnalViews();
    }

    public CommentCompactTweetView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        findAdditionnalViews();
    }

    /**
     * Method used by twitter library to retrieve related layout
     * @return
     */
    @Override
    protected int getLayout() {
        return R.layout.custom_tweet_compact;
    }

    /**
     * Retrieve views references
     */
    private void findAdditionnalViews() {
        tw__comment = (LinearLayout) findViewById(R.id.tw__comment);
        tw_comment1 = (LinearLayout) findViewById(R.id.tw_comment1);
        tw_comment2 = (LinearLayout) findViewById(R.id.tw_comment2);
        tw_comment3 = (LinearLayout) findViewById(R.id.tw_comment3);
        tw_comment1_pseudo = (TextView) tw_comment1.findViewById(R.id.pseudo);
        tw_comment2_pseudo = (TextView) tw_comment2.findViewById(R.id.pseudo);
        tw_comment3_pseudo = (TextView) tw_comment3.findViewById(R.id.pseudo);
        tw_comment1_message = (TextView) tw_comment1.findViewById(R.id.message);
        tw_comment2_message = (TextView) tw_comment2.findViewById(R.id.message);
        tw_comment3_message = (TextView) tw_comment3.findViewById(R.id.message);
        tw_details = (Button) findViewById(R.id.tweet_detail);
    }


    public void setMoreButtonClickListener(final OnMoreButtonClickListener moreButtonClickListener) {
        this.moreButtonClickListener = moreButtonClickListener;
        tw_details.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                moreButtonClickListener.onMoreButtonClick();
            }
        });
    }

    /**
     * Interface used to notify more button clicks events on a tweet
     */
    public interface OnMoreButtonClickListener {
        void onMoreButtonClick();
    }

    /**
     * reset comments views at their initial state (hidden without text)
     */
    public void resetCommentViews() {
        tw__comment.setVisibility(GONE);
        tw_comment1.setVisibility(GONE);
        tw_comment2.setVisibility(GONE);
        tw_comment3.setVisibility(GONE);
        tw_comment1_message.setText("");
        tw_comment2_message.setText("");
        tw_comment3_message.setText("");
        tw_comment1_pseudo.setText("");
        tw_comment2_pseudo.setText("");
        tw_comment3_pseudo.setText("");
    }
}
