package fr.enssat.gazouilli;

import com.google.gson.Gson;

import fr.enssat.gazouilli.model.CommentVO;

public class CommentFactory {

    public static final String JSON_TYPE = "JSON";
    public static final String CSV_TYPE = "CSV";

    public CommentVO getMessage(String source, String type) {
        if(type.equalsIgnoreCase(JSON_TYPE)) {
            return parsePrivateMessageFromJson(source);
        } else if(type.equalsIgnoreCase(CSV_TYPE)) {
            return parsePrivateMessageFromCSV(source);
        }
        return null;
    }

    private CommentVO parsePrivateMessageFromJson(String message) {
        Gson gson = new Gson();
        return gson.fromJson(message, CommentVO.class);
    }

    /**
     * TODO : à implémenter
     * @param message
     * @return
     */
    private CommentVO parsePrivateMessageFromCSV(String message) {
        CommentVO commentVO = null;
        return commentVO;
    }
}

