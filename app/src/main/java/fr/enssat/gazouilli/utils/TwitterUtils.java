package fr.enssat.gazouilli.utils;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.TwitterSession;

import fr.enssat.gazouilli.Constants;

/**
 * Twitter helper
 */
public class TwitterUtils {

    /**
     * Get active twitter session
     * @return
     */
    public static TwitterSession getActiveSession() {
        return Twitter.getSessionManager().getActiveSession();
    }

    /**
     * Logout user if connected to twitter
     */
    public static void logout() {
        if(isLogged()) {
            Twitter.getSessionManager().clearActiveSession();
            Twitter.logOut();
        }
    }

    /**
     * Check is user's is connected to twitter
     * @return
     */
    public static Boolean isLogged() {
        TwitterSession session = getActiveSession();
        return session != null;
    }

    /**
     * Gets connected user twitter's pseudo or empty
     * @return
     */
    public static String getUsername() {
        String pseudo = "";
        if(isLogged()) {
            pseudo = getActiveSession().getUserName();
        } else {
            pseudo = "";
        }
        return pseudo;
    }

    /**
     * Check if twitter official application is installed on user's terminal
     * @param context
     * @return
     */
    public static Boolean isTwitterInstalled(Context context) {
        PackageManager pkManager = context.getPackageManager();
        Boolean result = false;
        try {
            PackageInfo pkgInfo = pkManager.getPackageInfo("com.twitter.android", 0);
            result = true;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            result = false;
        }
        return result;
    }

    /**
     * Associate a text with a channel hashtag
     * ex : salut les amis ! #enssat
     * @param text
     * @param channelName
     * @return
     */
    public static String makeTweetText(String text, String channelName) {
        StringBuilder stringBuilder = new StringBuilder()
                .append(text)
                .append(" ")
                .append("#")
                .append(channelName);

        return stringBuilder.toString();
    }
}
