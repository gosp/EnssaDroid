package fr.enssat.gazouilli.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Network helper
 */
public abstract class NetworkUtils {

    /**
     * Check if user is network connected (wifi, data, ...)
     * @param context
     * @return
     */
    public static Boolean isConnected(Context context) {
        ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

        if(networkInfo != null && networkInfo.isConnected()){
            return true;
        }
        else
        {
            return false;
        }
    }
}
