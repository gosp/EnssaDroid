package fr.enssat.gazouilli.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Date helper
 */
public abstract class DateUtils {

    /**
     * Get current date as string of specific format
     * ex : 09-janvier-2016
     * @return
     */
    public static String getCurrentDate() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MMMM-yyyy");
        String strDate = sdf.format(c.getTime());
        return strDate;
    }
}
