package fr.enssat.gazouilli;

/**
 * Holds app constants
 */
public abstract class Constants {

    // App
    public static final Boolean DEBUG = false;

    // Twitter
    public static final String TWITTER_KEY = "SdYsaZOd8XGugosOYDBmCwPV9";
    public static final String TWITTER_SECRET = "VafH1ZucHhgQRuyRGax1WTHg3VAma1I9YERIa9cGNnzq5oWZQ5";
    public static final int TWITTER_REQUEST_CODE = 1;
    public static final String APP_CHANNEL_NAME = "enssat";
    public static final String TWITTER_SEARCH_RESULT_TYPE = "recent";
    public static final int TWITTER_SEARCH_COUNT = 100;
    public static final String TWITTER_EXCLUDE_RETWEETS = "-filter:retweets";

    // GCM
    // local project SENDER_ID for tests & debug
    public static final String LOCAL_SENDER_ID = "747680407299";
    // local project server url
    public static final String LOCAL_SERVER_URL = "https://0.0.0.0:8888/_ah/api/";
    // enssat project SENDER_ID
    public static final String ENSSAT_SENDER_ID = "2414251509";
    // enssat project serveur url
    public static final String ENSSAT_SERVER_URL = "https://perfect-stock-115314.appspot.com/_ah/api/";

    // success / error messages
    public static final String ERROR_DEFAULT = "Error";
    public static final String TEXT_INVALID = "_Invalid_";
    public static final String TEXT_SUCCESS_DEFAULT = "Success";
    public static final String ERROR_NETWORK_UNAVAILABLE = "Vérifiez votre connexion à internet";
    public static final String ERROR_NOT_LOGGEDIN = "Vous devez êtres connecté";

    // shared preferences
    public static final String APP_SHARED_PREFERENCES = "enssadroid";
    public static final String PREFERENCE_FIRST_TIME = "first_time";
    public static final String PREFERENCE_LAST_MESSAGE_RECEIVED_ID = "last_message_id_received";
    public static final String PREFERENCE_LAST_MESSAGE_SENT_ID = "last_message_id_sent";
    public static final String PREFERENCE_RETWEETS = "retweets";
    public static final String PREFERENCE_NOTIFICATION = "notification";
    public static final String PREFERENCE_APP_CHANNEL_NAME = "app_channel_name";
}
