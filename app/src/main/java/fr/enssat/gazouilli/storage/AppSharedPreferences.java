package fr.enssat.gazouilli.storage;

import android.content.Context;
import android.content.SharedPreferences;

import fr.enssat.gazouilli.Constants;

/**
 * Shared preferences helper
 */
public class AppSharedPreferences {

    private final Context context;

    public AppSharedPreferences(Context context) {
        this.context = context;
    }

    /**
     * Gets shared preferences file name specified by the app
     * @return
     */
    public SharedPreferences getAppSharedPreferences() {
        return context.getSharedPreferences(Constants.APP_SHARED_PREFERENCES, Context.MODE_PRIVATE);
    }

    /**
     * Check if it's the first time user use the app
     * @return
     */
    public Boolean isFirstTimeAppLaunched() {
        return getAppSharedPreferences().getBoolean(Constants.PREFERENCE_FIRST_TIME, true);
    }

    public void setFirstTimeAppLaunched(Boolean firstTimeAppLaunched) {
        getAppSharedPreferences().edit().putBoolean(Constants.PREFERENCE_FIRST_TIME, firstTimeAppLaunched).commit();
    }

    public void putLastMessageIdReceived(String messageId) {
        getAppSharedPreferences().edit().putString(Constants.PREFERENCE_LAST_MESSAGE_RECEIVED_ID, messageId).commit();
    }

    public String getLastMessageIdReceived() {
        return getAppSharedPreferences().getString(Constants.PREFERENCE_LAST_MESSAGE_RECEIVED_ID, "");
    }

    public void putLastMessageIdSent(String messageId) {
        getAppSharedPreferences().edit().putString(Constants.PREFERENCE_LAST_MESSAGE_SENT_ID, messageId).commit();
    }

    public String getLastMessageIdSent() {
        return getAppSharedPreferences().getString(Constants.PREFERENCE_LAST_MESSAGE_SENT_ID, "");
    }

    //-- retweets

    public Boolean isRetweetsEnabled() {
        return getAppSharedPreferences().getBoolean(Constants.PREFERENCE_RETWEETS, true);
    }

    public void setReTweets(Boolean reTweets) {
        getAppSharedPreferences().edit().putBoolean(Constants.PREFERENCE_RETWEETS, reTweets).commit();
    }

    //-- app channel name

    public String getAppChannelName() {
        return getAppSharedPreferences().getString(Constants.PREFERENCE_APP_CHANNEL_NAME, Constants.APP_CHANNEL_NAME);
    }

    public void setAppChannelName(String appChannelName) {
        getAppSharedPreferences().edit().putString(Constants.PREFERENCE_APP_CHANNEL_NAME, appChannelName).commit();
    }

    //-- notifications

    public Boolean isNotificationEnabled() {
        return getAppSharedPreferences().getBoolean(Constants.PREFERENCE_NOTIFICATION, true);
    }

    public void setNotification(Boolean notification) {
        getAppSharedPreferences().edit().putBoolean(Constants.PREFERENCE_NOTIFICATION, notification).commit();
    }
}
