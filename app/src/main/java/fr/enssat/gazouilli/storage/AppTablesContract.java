package fr.enssat.gazouilli.storage;

import android.provider.BaseColumns;

/**
 * App database tables definition
 */
public class AppTablesContract {

    /**
     * "private_message" database table definition
     */
    public static abstract class PrivateMessage implements BaseColumns {
        public static final String TABLE_NAME = "private_messages";
        public static final String COLUM_NAME_ID = "id";
        public static final String COLUMN_NAME_MESSAGE_ID = "message_id";
        public static final String COLUMN_NAME_USER_PSEUDO = "user_pseudo";
        public static final String COLUMN_NAME_DATE = "date";
        public static final String COLUMN_NAME_MESSAGE = "message";
        public static final String COLUMN_NAME_TWEETID = "tweet_id";
    }
}
