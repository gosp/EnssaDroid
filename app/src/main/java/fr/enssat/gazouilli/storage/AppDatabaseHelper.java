package fr.enssat.gazouilli.storage;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

import fr.enssat.gazouilli.model.CommentVO;

/**
 * Define app database and its tables
 * Also provide methods (CRUD) to manage database table content (create, read, update, delete)
 */
public class AppDatabaseHelper extends SQLiteOpenHelper {

    // database version and name
    public static final int DATABASE_VERSION = 2;
    public static final String DATABASE_NAME = "AppReader.db";

    // database types
    private static final String TEXT_TYPE = " TEXT";
    private static final String INTEGER_TYPE = " INTEGER";
    private static final String BOOLEAN_TYPE = " BOOLEAN";
    private static final String COMMA_SEP = ",";

    /**
     * SQL statement to create the database structure
     */
    public static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + AppTablesContract.PrivateMessage.TABLE_NAME + " (" +
                    AppTablesContract.PrivateMessage.COLUM_NAME_ID + " INTEGER PRIMARY KEY, " +
                    AppTablesContract.PrivateMessage.COLUMN_NAME_MESSAGE_ID + TEXT_TYPE + COMMA_SEP +
                    AppTablesContract.PrivateMessage.COLUMN_NAME_USER_PSEUDO + TEXT_TYPE + COMMA_SEP +
                    AppTablesContract.PrivateMessage.COLUMN_NAME_DATE + TEXT_TYPE + COMMA_SEP +
                    AppTablesContract.PrivateMessage.COLUMN_NAME_MESSAGE + TEXT_TYPE + COMMA_SEP +
                    AppTablesContract.PrivateMessage.COLUMN_NAME_TWEETID + INTEGER_TYPE +
                    " )";

    /**
     * SQL statement to delete database tables entries
     */
    private static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + AppTablesContract.PrivateMessage.TABLE_NAME;

    /**
     * Constructor
     * @param context
     */
    public AppDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    /**
     * SQL database create method
     * @param db
     */
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_ENTRIES);
    }

    /**
     * SQL database update method
     * @param db
     * @param oldVersion
     * @param newVersion
     */
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // This database is only a cache for online data, so its upgrade policy is
        // to simply to discard the data and start over
        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
    }

    /**
     * SQL database downgrade method
     * @param db
     * @param oldVersion
     * @param newVersion
     */
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

    //-- Specific "private_message" table methods (CRUD)

    /**
     * Add provided private message
     * @param commentVO
     */
    public void addPrivateMessage(CommentVO commentVO) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(AppTablesContract.PrivateMessage.COLUMN_NAME_MESSAGE_ID, commentVO.messageID);
        values.put(AppTablesContract.PrivateMessage.COLUMN_NAME_USER_PSEUDO, commentVO.userPseudo);
        values.put(AppTablesContract.PrivateMessage.COLUMN_NAME_TWEETID, commentVO.tweetID);
        values.put(AppTablesContract.PrivateMessage.COLUMN_NAME_DATE, commentVO.date);
        values.put(AppTablesContract.PrivateMessage.COLUMN_NAME_MESSAGE, commentVO.message);
        db.insert(AppTablesContract.PrivateMessage.TABLE_NAME, null, values);
        db.close();
    }

    /**
     * Retrieve tweet of id specified
     * @param tweetId
     * @return
     */
    public List<CommentVO> getPrivateMessageListFromTweet(long tweetId) {
        SQLiteDatabase db = this.getReadableDatabase();
        List<CommentVO> commentVOList = new ArrayList<>();
        CommentVO commentVO = null;

        String[] projection = {
                AppTablesContract.PrivateMessage.COLUM_NAME_ID,
                AppTablesContract.PrivateMessage.COLUMN_NAME_MESSAGE_ID,
                AppTablesContract.PrivateMessage.COLUMN_NAME_USER_PSEUDO,
                AppTablesContract.PrivateMessage.COLUMN_NAME_DATE,
                AppTablesContract.PrivateMessage.COLUMN_NAME_TWEETID,
                AppTablesContract.PrivateMessage.COLUMN_NAME_MESSAGE
        };

        Cursor cursor = db.query(AppTablesContract.PrivateMessage.TABLE_NAME,
                projection, AppTablesContract.PrivateMessage.COLUMN_NAME_TWEETID + "=?", new String[] { String.valueOf(tweetId) }, null, null, null);

        while (cursor.moveToNext()) {
            commentVO = new CommentVO(
                    cursor.getString(1),
                    cursor.getString(2),
                    cursor.getString(3),
                    cursor.getInt(4),
                    cursor.getString(5));

            commentVOList.add(commentVO);
        }

        cursor.close();
        db.close();

        return commentVOList;
    }

    /**
     * Get 3 last comments of the tweet id specified
     * @param tweetId
     * @return
     */
    public List<CommentVO> getThreeLastPrivateMessageListFromTweet(long tweetId) {
        SQLiteDatabase db = this.getReadableDatabase();
        List<CommentVO> commentVOList = new ArrayList<>();
        CommentVO commentVO = null;

        String[] projection = {
                AppTablesContract.PrivateMessage.COLUM_NAME_ID,
                AppTablesContract.PrivateMessage.COLUMN_NAME_MESSAGE_ID,
                AppTablesContract.PrivateMessage.COLUMN_NAME_USER_PSEUDO,
                AppTablesContract.PrivateMessage.COLUMN_NAME_DATE,
                AppTablesContract.PrivateMessage.COLUMN_NAME_TWEETID,
                AppTablesContract.PrivateMessage.COLUMN_NAME_MESSAGE
        };

        Cursor cursor = db.query(AppTablesContract.PrivateMessage.TABLE_NAME,
                projection, AppTablesContract.PrivateMessage.COLUMN_NAME_TWEETID + "=?", new String[] { String.valueOf(tweetId) }, null, null, AppTablesContract.PrivateMessage.COLUM_NAME_ID + " DESC", "3");

        while (cursor.moveToNext()) {
            commentVO = new CommentVO(
                    cursor.getString(1),
                    cursor.getString(2),
                    cursor.getString(3),
                    cursor.getInt(4),
                    cursor.getString(5));

            commentVOList.add(commentVO);
        }

        cursor.close();
        db.close();

        return commentVOList;
    }

    /**
     * Delete comments related to the tweet id provided
     * @param tweetId
     */
    public void deleteAllCommentsFromTweetId(int tweetId) {
        SQLiteDatabase db = this.getReadableDatabase();

        db.delete(
                AppTablesContract.PrivateMessage.TABLE_NAME,
                AppTablesContract.PrivateMessage.COLUMN_NAME_TWEETID + "=?",
                new String[]{String.valueOf(tweetId)});
    }

    /**
     * Delete all tweets comments
     */
    public void deleteAllComments() {
        SQLiteDatabase db = this.getReadableDatabase();
        db.delete(AppTablesContract.PrivateMessage.TABLE_NAME, null, null);
    }
}
