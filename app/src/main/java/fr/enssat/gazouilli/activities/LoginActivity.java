package fr.enssat.gazouilli.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;

import fr.enssat.gazouilli.R;
import fr.enssat.gazouilli.utils.TwitterUtils;

/**
 * Activity which displays a choice to the user to connect
 * with a twitter account or use the application as guest
 */
public class LoginActivity extends BaseActivity {

    // Views references
    private TwitterLoginButton loginButton;
    private Button guestButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        // retrieve views references
        guestButton = (Button) findViewById(R.id.btn_guest_mode);
        loginButton = (TwitterLoginButton) findViewById(R.id.twitter_login_button);

        // start home activity if login successful
        loginButton.setCallback(new Callback<TwitterSession>() {
            @Override
            public void success(Result<TwitterSession> result) {
                TwitterSession session = result.data;
                String message = getResources().getString(R.string.connection_successful);
                displayMessage(message + "@" + session.getUserName());
                startHomeActivity();
            }

            // else display error message
            @Override
            public void failure(TwitterException exception) {
                Log.d("TwitterKit", "Login with Twitter failure", exception);
                String message = getResources().getString(R.string.connection_failure);
                displayError(message);
            }
        });

        guestButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(TwitterUtils.isLogged()) {
                    startHomeActivity();
                } else {
                    startGuestHomeActivity();
                }
            }
        });
    }

    @Override
    public void startHomeActivity() {
        startActivity(new Intent(LoginActivity.this, HomeActivity.class));
        finish();
    }

    @Override
    public void startGuestHomeActivity() {
        startActivity(new Intent(LoginActivity.this, GuestHomeActivity.class));
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Pass the activity result to the login button.
        loginButton.onActivityResult(requestCode, resultCode, data);
    }
}

