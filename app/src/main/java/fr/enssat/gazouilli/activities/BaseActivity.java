package fr.enssat.gazouilli.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;

import fr.enssat.gazouilli.Constants;
import fr.enssat.gazouilli.gcm.GcmRegistrationAsyncTask;
import fr.enssat.gazouilli.utils.NetworkUtils;
import fr.enssat.gazouilli.utils.TwitterUtils;
import io.fabric.sdk.android.Fabric;

/**
 * Parent activity of every activities (except MainActivity) of this app which common methods
 */
public abstract class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // If there isn't any network connection, then redirect to main activity
        if(!NetworkUtils.isConnected(getApplicationContext())) {
            redirectMain();
        }
    }

    /**
     * Display an error message
     * @param text
     */
    public void displayError(String text) {
        if(text.length() < 0) {
            text = Constants.ERROR_DEFAULT;
        }
        Toast.makeText(BaseActivity.this, text, Toast.LENGTH_SHORT).show();
    }

    // Display a message
    public void displayMessage(String text) {
        if(text.length() < 0) {
            text = Constants.TEXT_SUCCESS_DEFAULT;
        }
        Toast.makeText(BaseActivity.this, text, Toast.LENGTH_SHORT).show();
    }

    public void redirectMain() {
        startActivity(new Intent(this, MainActivity.class));
    }

    public void startLoginActivity() {
        startActivity(new Intent(this, LoginActivity.class));
    }

    public void startHomeActivity() {
        startActivity(new Intent(this, HomeActivity.class));
    }

    public void startGuestHomeActivity() {
        startActivity(new Intent(this, GuestHomeActivity.class));
    }

    public void startSettingsActivity() {
        startActivity(new Intent(this, SettingsActivity.class));
    }

    /**
     * Disconnect user and redirect him to the main activity
     */
    public void twitterLogout() {
        TwitterUtils.logout();
        redirectMain();
    }
}
