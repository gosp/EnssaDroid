package fr.enssat.gazouilli.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;

import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;

import android.os.Handler;

import fr.enssat.gazouilli.storage.AppSharedPreferences;
import fr.enssat.gazouilli.gcm.GcmRegistrationAsyncTask;
import fr.enssat.gazouilli.R;
import fr.enssat.gazouilli.utils.NetworkUtils;
import fr.enssat.gazouilli.utils.TwitterUtils;
import io.fabric.sdk.android.Fabric;

/**
 * Main entry of the application. This activity register the app on GCM server
 * and setup fabrics keys. Finally it redirect user's on others app locations
 */
public class MainActivity extends AppCompatActivity {

    // req server : { 	"userID": 12, 	"userPseudo": "Gosp", 	"userDate": "12-08-2015", 	"tweetID": 682258308716179457, 	"message": "lol les enfants" }

    // { 	"userID": 12, 	"userPseudo": "Gosp", 	"userDate": "12-08-2015", 	"tweetID":683416130799710208, 	"message": "lol les enfants" }

    // Twitter's fabrics public and secret keys related to this app
    private static final String TWITTER_KEY = "SdYsaZOd8XGugosOYDBmCwPV9";
    private static final String TWITTER_SECRET = "VafH1ZucHhgQRuyRGax1WTHg3VAma1I9YERIa9cGNnzq5oWZQ5";

    // views references
    private Button retryButton;
    private ProgressBar loader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // retrieve views references
        retryButton = (Button) findViewById(R.id.retryButton);
        loader = (ProgressBar) findViewById(R.id.loader);

        final Handler handler = new Handler();

        retryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loader.setVisibility(View.VISIBLE);
                handler.postDelayed(new Runnable() {
                    public void run() {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                loader.setVisibility(View.GONE);
                                if(NetworkUtils.isConnected(getApplicationContext())) {
                                    recreate();
                                }
                            }
                        });
                    }
                }, 2000);
            }
        });

        // If user is network connected
        if(NetworkUtils.isConnected(getApplicationContext())) {
            // register GCM
            new GcmRegistrationAsyncTask(this).execute();

            // register with Twitter's fabric
            TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
            Fabric.with(this, new Twitter(authConfig));

            // if first time app launched, display login activity
            AppSharedPreferences appSharedPreferences = new AppSharedPreferences(getApplicationContext());
            if(appSharedPreferences.isFirstTimeAppLaunched()) {
                appSharedPreferences.setFirstTimeAppLaunched(false);
                startLoginActivity();
            } else {
                // if user comes from notification
                if(getIntent() !=null && getIntent().hasExtra(TweetActivity.INTENT_EXTRA_TWEET_ID)) {
                    startTweetActivity(getIntent().getLongExtra(TweetActivity.INTENT_EXTRA_TWEET_ID, 0));
                } else { // else, start home page
                    if(TwitterUtils.isLogged()) {
                        startHomeActivity();
                    } else {
                        startGuestHomeActivity();
                    }
                }
            }
        } else { // display a splashscreen with retry button until user's get network connection access back
            retryButton.setVisibility(View.VISIBLE);
        }
    }

    public void startLoginActivity() {
        startActivity(new Intent(MainActivity.this, LoginActivity.class));
    }

    public void startHomeActivity() {
        startActivity(new Intent(MainActivity.this, HomeActivity.class));
    }

    public void startGuestHomeActivity() {
        startActivity(new Intent(MainActivity.this, GuestHomeActivity.class));
    }

    /**
     * Asks TweetActivity to display tweet of given id
     * @param tweetId
     */
    public void startTweetActivity(long tweetId) {
        Intent intent = new Intent(MainActivity.this, TweetActivity.class);
        intent.putExtra(TweetActivity.INTENT_EXTRA_TWEET_ID, tweetId);
        startActivity(intent);
    }
}
