package fr.enssat.gazouilli.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.models.Tweet;
import com.twitter.sdk.android.tweetui.TweetUtils;
import com.twitter.sdk.android.tweetui.TweetView;

import java.util.List;
import java.util.UUID;

import fr.enssat.gazouilli.storage.AppSharedPreferences;
import fr.enssat.gazouilli.adapters.CommentAdapter;
import fr.enssat.gazouilli.gcm.GcmSendMessageAsyncTask;
import fr.enssat.gazouilli.model.CommentVO;
import fr.enssat.gazouilli.storage.AppDatabaseHelper;
import fr.enssat.gazouilli.R;
import fr.enssat.gazouilli.utils.DateUtils;
import fr.enssat.gazouilli.utils.NetworkUtils;
import fr.enssat.gazouilli.utils.TwitterUtils;

/**
 * This activity displays a specific tweet and all its comments
 * User can add a comment to this tweet and show incoming comments of this tweet
 * in real-time.
 */
public class TweetActivity extends BaseActivity {

    // views references
    public EditText newCommentContent;
    public Button newCommentButton;
    private LinearLayout newCommentContainer;
    private ListView commentListView;
    private ProgressBar loader;

    // Tweet id used by this activity
    private long tweetId;

    // database helper reference
    private AppDatabaseHelper appDatabaseHelper;

    // Adapter which displays a list of comments
    private CommentAdapter commentAdapter;

    // Intent extra used to send a tweet id to display
    public static final String INTENT_EXTRA_TWEET_ID = "tweetId";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // inflate view & retrieve references
        setContentView(R.layout.activity_tweet);
        newCommentButton = (Button) findViewById(R.id.new_message_button);
        newCommentContent = (EditText) findViewById(R.id.new_message_content);
        newCommentContainer = (LinearLayout) findViewById(R.id.new_message_container);
        commentListView = (ListView) findViewById(R.id.comment_list);
        loader = (ProgressBar) findViewById(R.id.loader);

        //enable toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // retrieve the Tweet ID passed in
        Intent intent = getIntent();
        tweetId = 682571848551395328L;
        if(intent != null) {
            tweetId = intent.getLongExtra("tweetId", 631879971628183552L);
        }

        // get database helper
        appDatabaseHelper = new AppDatabaseHelper(getApplicationContext());

        // retrieve all comments related to a tweet id
        List<CommentVO> commentVOList = appDatabaseHelper.getPrivateMessageListFromTweet(tweetId);

        // init comment adapter
        commentAdapter = new CommentAdapter(getApplicationContext(), R.layout.private_message_item, commentVOList);

        /**
         * starts loading tweets and fulfills the adapter with the results
         * and displays tweet content
         */
        TweetUtils.loadTweet(tweetId, new Callback<Tweet>() {
            @Override
            public void success(Result<Tweet> result) {
                TweetView tweetView = new TweetView(TweetActivity.this, result.data);
                commentListView.addHeaderView(tweetView);
                commentListView.setAdapter(commentAdapter);
                hideLoader();
            }

            @Override
            public void failure(TwitterException exception) {
                Log.d("TwitterKit", "Load Tweet failure", exception);
                hideLoader();
            }
        });

        /**
         * Listen to new message input view clicks events
         * Add new comment to the tweet
         */
        newCommentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (NetworkUtils.isConnected(getApplicationContext())) {
                    if (TwitterUtils.isLogged()) {
                        if (!getNewCommentContent().equals("")) {
                            // create UUID
                            String uniqueID = UUID.randomUUID().toString();
                            CommentVO commentVO = new CommentVO.Builder(uniqueID, TwitterUtils.getUsername(), tweetId, getNewCommentContent())
                                    .setDate(DateUtils.getCurrentDate())
                                    .build();
                            resetNewMessageContent();
                            scrollMyListViewToBottom();
                            new GcmSendMessageAsyncTask(getApplicationContext()).execute(commentVO);
                            displayMessage(getString(R.string.tweet_send_success));
                        } else {
                            displayError(getString(R.string.tweet_content_invalid));
                        }
                    } else {
                        displayError(getString(R.string.twitter_account_required));
                    }
                } else {
                    displayError(getString(R.string.network_error));
                }
            }
        });

        AppSharedPreferences appSharedPreferences = new AppSharedPreferences(getApplicationContext());

        // set actionbar title to tweet channel specified
        setTitle(appSharedPreferences.getAppChannelName());

        // if user connected to twitter, display new comment input view
        if(TwitterUtils.isLogged()) {
            newCommentContainer.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void onPause() {
        // unregister to incoming tweets comments events
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
        super.onPause();
    }

    @Override
    protected void onResume() {
        // register to incoming tweets comments events
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver, new IntentFilter("new-comment"));
        super.onResume();
    }

    /**
     * Retrieve text from new comment input view
     * @return
     */
    public String getNewCommentContent() {
        String result = "";
        if (newCommentContent.getText().length() > 0) {
            result = newCommentContent.getText().toString();
        }
        return result;
    }

    /**
     * Delete new message input view content
     */
    public void resetNewMessageContent() {
        newCommentContent.setText("");
    }

    // Broadcast receiver used to listen to incoming comments events
    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d("receiver", "Got message: ");
            refreshAllComments();
        }
    };

    // refresh adapter's content
    private void refreshAllComments() {
        appDatabaseHelper = new AppDatabaseHelper(getApplicationContext());
        List<CommentVO> commentVOList = appDatabaseHelper.getPrivateMessageListFromTweet(tweetId);
        commentAdapter.refresh(commentVOList);
    }

    // scroll the comment listview to the bottom (last comment)
    private void scrollMyListViewToBottom() {
        commentListView.post(new Runnable() {
            @Override
            public void run() {
                // Select the last row so it will scroll into view...
                commentListView.setSelection(commentAdapter.getCount() - 1);
            }
        });
    }

    // hide loader icon
    private void hideLoader() {
        loader.setVisibility(View.GONE);
    }
}
