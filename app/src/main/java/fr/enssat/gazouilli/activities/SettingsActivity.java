package fr.enssat.gazouilli.activities;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Toast;
import android.widget.ToggleButton;

import fr.enssat.gazouilli.storage.AppSharedPreferences;
import fr.enssat.gazouilli.storage.AppDatabaseHelper;
import fr.enssat.gazouilli.R;

/**
 * This activity displays a list of settings related to the application
 */
public class SettingsActivity extends BaseActivity {

    //views references
    Button resetDatabaseBtn;

    // database helper
    private AppDatabaseHelper appDatabaseHelper;
    private ToggleButton activateRetweetsButton;
    private ToggleButton activateNotificationsButton;
    private EditText channelName;
    private AppSharedPreferences appSharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        //retrieve views references
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        resetDatabaseBtn = (Button) findViewById(R.id.btnDeleteAllComments);
        activateRetweetsButton = (ToggleButton) findViewById(R.id.btn_activate_retweets);
        activateNotificationsButton = (ToggleButton) findViewById(R.id.btn_activate_notifications);
        channelName = (EditText) findViewById(R.id.channel_name);

        // get database helper
        appDatabaseHelper = new AppDatabaseHelper(getApplicationContext());

        // get App shared preferences
        appSharedPreferences = new AppSharedPreferences(getApplicationContext());

        resetDatabaseBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                appDatabaseHelper.deleteAllComments();
                Toast.makeText(getApplicationContext(), "tous les commentaires ont étés supprimés avec succès", Toast.LENGTH_SHORT).show();
            }
        });

        channelName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0 && !s.toString().equals("")) {
                    appSharedPreferences.setAppChannelName(s.toString());
                }
            }
        });

        activateRetweetsButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                appSharedPreferences.setReTweets(isChecked);
            }
        });

        activateNotificationsButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                appSharedPreferences.setNotification(isChecked);
            }
        });

        initSettings();
    }

    private void initSettings() {
        channelName.setText(appSharedPreferences.getAppChannelName());
        activateRetweetsButton.setChecked(appSharedPreferences.isRetweetsEnabled());
        activateNotificationsButton.setChecked(appSharedPreferences.isNotificationEnabled());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_settings, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }
}
