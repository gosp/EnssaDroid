package fr.enssat.gazouilli.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AbsListView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.models.Tweet;
import com.twitter.sdk.android.tweetui.SearchTimeline;
import com.twitter.sdk.android.tweetui.TimelineResult;

import java.lang.ref.WeakReference;

import fr.enssat.gazouilli.storage.AppSharedPreferences;
import fr.enssat.gazouilli.Constants;
import fr.enssat.gazouilli.adapters.CommentTweetTimelineAdapter;
import fr.enssat.gazouilli.R;
import fr.enssat.gazouilli.utils.TwitterUtils;
import io.fabric.sdk.android.Fabric;

/**
 * Activity which display a limited list of tweet based on channel specified by the application
 */
public class GuestHomeActivity extends BaseActivity implements CommentTweetTimelineAdapter.OnMoreButtonClickListener {

    final WeakReference<Activity> activityRef = new WeakReference<Activity>(GuestHomeActivity.this);

    private String channeName = Constants.APP_CHANNEL_NAME;
    private String channelQuery = channeName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TwitterAuthConfig authConfig = new TwitterAuthConfig(Constants.TWITTER_KEY, Constants.TWITTER_SECRET);
        Fabric.with(this, new Twitter(authConfig));
        setContentView(R.layout.activity_guest_home);

        // retrieve views references
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        TextView emptyText = (TextView)findViewById(android.R.id.empty);
        final SwipeRefreshLayout swipeLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_layout);
        final ListView list = (ListView) findViewById(android.R.id.list);

        // set channel query (with parameters if exist)
        AppSharedPreferences appSharedPreferences = new AppSharedPreferences(getApplicationContext());
        channelQuery = appSharedPreferences.getAppChannelName();

        // build & setup tweet timeline with app channel
        /*final UserTimeline userTimeline = new UserTimeline.Builder()
                .screenName(channelQuery)
                .build();*/
        final SearchTimeline searchTimeline = new SearchTimeline.Builder()
                .query(channelQuery)
                .build();
        final CommentTweetTimelineAdapter adapter = new CommentTweetTimelineAdapter(this, searchTimeline);

        list.setAdapter(adapter);
        list.setEmptyView(emptyText);
        adapter.setMoreButtonClickListener(this);

        // detect swipe and trigger a refresh action at list top
        list.setOnScrollListener(new AbsListView.OnScrollListener() {
            boolean enableRefresh = false;

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (list != null && list.getChildCount() > 0) {
                    // check that the first item is visible and that its top matches the parent
                    enableRefresh = list.getFirstVisiblePosition() == 0 && list.getChildAt(0).getTop() >= 0;
                } else {
                    enableRefresh = false;
                }
                swipeLayout.setEnabled(enableRefresh);
            }
        });

        // refresh timeline tweet on swipe
        swipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeLayout.setRefreshing(true);
                adapter.refresh(new Callback<TimelineResult<Tweet>>() {
                    @Override
                    public void success(Result<TimelineResult<Tweet>> result) {
                        swipeLayout.setRefreshing(false);
                    }

                    @Override
                    public void failure(TwitterException exception) {
                        swipeLayout.setRefreshing(false);
                        final Activity activity = activityRef.get();
                        if (activity != null && !activity.isFinishing()) {
                            Toast.makeText(activity, exception.getMessage(),
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        });

        // set actionbar title to tweet channel specified
        setTitle(appSharedPreferences.getAppChannelName());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            startSettingsActivity();
            return true;
        } else if(id == R.id.action_disconnect) {
            twitterLogout();
            finish();
        } else if(id == R.id.action_connect) {
            startLoginActivity();
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onTweetMoreButtonClick(Long tweetId) {
        startTweetActivity(tweetId);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        MenuItem pseudoItem = menu.findItem(R.id.pseudo);
        MenuItem connectItem = menu.findItem(R.id.action_connect);
        MenuItem disconnectItem = menu.findItem(R.id.action_disconnect);
        if(TwitterUtils.isLogged()) {
            pseudoItem.setTitle(TwitterUtils.getUsername());
            pseudoItem.setVisible(true);
            connectItem.setVisible(false);
            disconnectItem.setVisible(true);
        } else {
            pseudoItem.setVisible(false);
            connectItem.setVisible(true);
            disconnectItem.setVisible(false);
        }
        return true;
    }

    public void startTweetActivity(long tweetId) {
        Intent intent = new Intent(GuestHomeActivity.this, TweetActivity.class);
        intent.putExtra(TweetActivity.INTENT_EXTRA_TWEET_ID, tweetId);
        startActivity(intent);
    }
}