package fr.enssat.gazouilli.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterApiClient;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.models.Search;
import com.twitter.sdk.android.core.models.Tweet;
import com.twitter.sdk.android.core.services.SearchService;
import com.twitter.sdk.android.core.services.StatusesService;
import com.twitter.sdk.android.tweetcomposer.TweetComposer;
import com.twitter.sdk.android.tweetui.FixedTweetTimeline;

import java.util.List;

import fr.enssat.gazouilli.storage.AppSharedPreferences;
import fr.enssat.gazouilli.Constants;
import fr.enssat.gazouilli.adapters.CommentTweetTimelineAdapter;
import fr.enssat.gazouilli.R;
import fr.enssat.gazouilli.utils.NetworkUtils;
import fr.enssat.gazouilli.utils.TwitterUtils;

/**
 * Activity which displays a list of tweets and related comments (3 per tweets at most)
 * from the channel specified by the application for a user logged into twitter.
 * User can also post a tweet
 */
public class HomeActivity extends BaseActivity implements CommentTweetTimelineAdapter.OnMoreButtonClickListener {

    // views references
    private Button TweetButton;
    private EditText TweetContent;
    private LinearLayout TweetInputContainer;
    private CommentTweetTimelineAdapter adapter;
    private ListView list;

    // internal requests codes
    private static final int TWEETER_REQUEST_TWEET_CODE = 1;

    private String channeName = Constants.APP_CHANNEL_NAME;
    private String channelQuery = channeName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //TwitterAuthConfig authConfig = new TwitterAuthConfig(Constants.TWITTER_KEY, Constants.TWITTER_SECRET);
        //Fabric.with(this, new Twitter(authConfig));
        setContentView(R.layout.activity_home);

        // retrieve views references
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        TextView emptyText = (TextView)findViewById(android.R.id.empty);
        TweetButton = (Button) findViewById(R.id.new_message_button);
        TweetContent = (EditText) findViewById(R.id.new_message_content);
        TweetInputContainer = (LinearLayout) findViewById(R.id.new_message_container);
        final SwipeRefreshLayout swipeLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_layout);
        list = (ListView) findViewById(android.R.id.list);
        list.setEmptyView(emptyText);

        // set channel query (with parameters if exist)
        AppSharedPreferences appSharedPreferences = new AppSharedPreferences(getApplicationContext());
        channelQuery = appSharedPreferences.getAppChannelName();

        // include retweets ?
        if(!appSharedPreferences.isRetweetsEnabled()) {
            channelQuery += " "+Constants.TWITTER_EXCLUDE_RETWEETS;
        }

        // set actionbar title to tweet channel specified
        setTitle(appSharedPreferences.getAppChannelName());

        loadTweets(null);

        /**
         * Listen for "send a new tweet" action button
         */
        TweetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(NetworkUtils.isConnected(getApplicationContext())) {
                    if(!getTweetInputContent().equals(Constants.TEXT_INVALID)) {
                        // send tweet
                        startComposeTweet(getTweetInputContent());
                    } else {
                        displayError(getString(R.string.tweet_content_invalid));
                    }
                } else {
                    displayError(Constants.ERROR_NETWORK_UNAVAILABLE);
                }
            }
        });

        /**
         * detects swipe and trigger a refresh action at list top
         */
        list.setOnScrollListener(new AbsListView.OnScrollListener() {
            boolean enableRefresh = false;

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (list != null && list.getChildCount() > 0) {
                    // check that the first item is visible and that its top matches the parent
                    enableRefresh = list.getFirstVisiblePosition() == 0 && list.getChildAt(0).getTop() >= 0;
                } else {
                    enableRefresh = false;
                }
                // start refresh tweets list
                swipeLayout.setEnabled(enableRefresh);
            }
        });

        /**
         * display a refresh icon on top of listview
         */
        swipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeLayout.setRefreshing(true);
                // load tweets
                loadTweets(new LoadTweetsCallback() {
                    @Override
                    public void success(List<Tweet> tweets) {
                        displayMessage(getString(R.string.no_new_tweet));
                        swipeLayout.setRefreshing(false);
                    }

                    @Override
                    public void failure() {
                        swipeLayout.setRefreshing(false);
                        displayMessage(getString(R.string.refresh_tweet_error));
                    }
                });
            }
        });

        setupViews();
    }

    /**
     * Bind events on menu items
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            startSettingsActivity();
            return true;
        } else if(id == R.id.action_disconnect) {
            twitterLogout();
            finish();
        } else if(id == R.id.action_connect) {
            startLoginActivity();
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Sends tweet with provided text on twitter into channel specified by the application
     * If Twitter's app exists on user's terminal, then use it for sending the tweet
     * Otherwise, send the tweet directly using Twitter's API.
     * @param text
     */
    private void startComposeTweet(String text) {
        if(NetworkUtils.isConnected(getApplicationContext()) && TwitterUtils.isLogged()) {
            text = TwitterUtils.makeTweetText(text, channeName);
            if(TwitterUtils.isTwitterInstalled(getApplicationContext())) {
                // ask twitter app to send our new tweet
                Intent i = new TweetComposer.Builder(this).text(text).createIntent();
                startActivityForResult(i, TWEETER_REQUEST_TWEET_CODE);
            } else {
                //send tweet directly
                tweet(text);
            }
        } else {
            displayError(Constants.ERROR_NETWORK_UNAVAILABLE + " ou "+ Constants.ERROR_NOT_LOGGEDIN);
        }
    }

    /**
     * Starts TweetActivity of the tweet more button clicked by providing its id
     * @param tweetId
     */
    @Override
    public void onTweetMoreButtonClick(Long tweetId) {
        startTweetActivity(tweetId);
    }

    /**
     * Display menu items based on context conditions (network, user logged or not, ...)
     * @param menu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        MenuItem pseudoItem = menu.findItem(R.id.pseudo);
        MenuItem connectItem = menu.findItem(R.id.action_connect);
        MenuItem disconnectItem = menu.findItem(R.id.action_disconnect);
        //In case user is connected, display pseudo and disconnect button menu item
        if(TwitterUtils.isLogged()) {
            pseudoItem.setTitle(TwitterUtils.getUsername());
            pseudoItem.setVisible(true);
            connectItem.setVisible(false);
            disconnectItem.setVisible(true);
        } else { // else, just display connect button
            pseudoItem.setVisible(false);
            connectItem.setVisible(true);
            disconnectItem.setVisible(false);
        }
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == TWEETER_REQUEST_TWEET_CODE) {
            if(resultCode == RESULT_OK) {
                displayMessage(getString(R.string.tweet_send_success));
            }
            resetTweetInputContent();
        }
    }

    /**
     * Sends a tweet with provided text on twitter into the channel specified by the application
     * @param text
     */
    public void tweet(String text) {
        TwitterApiClient twitterApiClient = TwitterCore.getInstance().getApiClient(TwitterUtils.getActiveSession());
        StatusesService statusesService = twitterApiClient.getStatusesService();
        statusesService.update(text, null, null, null, null, null, null, null, new Callback<Tweet>() {
            @Override
            public void success(Result<Tweet> tweetResult) {
                displayMessage(getString(R.string.tweet_send_success));
                resetTweetInputContent();
            }

            @Override
            public void failure(TwitterException e) {
                e.printStackTrace();
                displayMessage(getString(R.string.tweet_not_sent));
            }
        });
    }

    /**
     * Show / hide activity views based on network and login conditions
     */
    public void setupViews() {
        // show new tweet input if user connected to twitter
        if(TwitterUtils.isLogged()) {
            TweetInputContainer.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Asks TweetActivity to display tweet of given id
     * @param tweetId
     */
    public void startTweetActivity(long tweetId) {
        Intent intent = new Intent(HomeActivity.this, TweetActivity.class);
        intent.putExtra(TweetActivity.INTENT_EXTRA_TWEET_ID, tweetId);
        startActivity(intent);
    }

    /**
     * Get text content of the new tweet input view
     * @return
     */
    public String getTweetInputContent() {
        String content = Constants.TEXT_INVALID;
        if(TweetContent.length() > 0) {
            content = TweetContent.getText().toString();
        }
        // TODO : delete spaces & undesirable characters
        return content;
    }

    /**
     * reset text content of the new tweet input view to it's initial value
     */
    public void resetTweetInputContent() {
        TweetContent.setHint(getString(R.string.your_tweet));
        TweetContent.setText("");
    }

    /**
     * Search for tweets and retrieve them
     * @param loadTweetsCallback
     */
    private void loadTweets(final LoadTweetsCallback loadTweetsCallback) {
        final SearchService service = Twitter.getApiClient().getSearchService();
        service.tweets(channelQuery, null, null, null,
                Constants.TWITTER_SEARCH_RESULT_TYPE, Constants.TWITTER_SEARCH_COUNT, null, null,
                null, true, new Callback<Search>() {
                    @Override
                    public void success(Result<Search> searchResult) {
                        final List<Tweet> tweets = searchResult.data.tweets;
                        final FixedTweetTimeline timeline = new FixedTweetTimeline.Builder()
                                .setTweets(tweets)
                                .build();
                        adapter = new CommentTweetTimelineAdapter(HomeActivity.this, timeline);
                        list.setAdapter(adapter);
                        adapter.setMoreButtonClickListener(HomeActivity.this);
                        adapter.notifyDataSetChanged();

                        if(loadTweetsCallback != null) {
                            loadTweetsCallback.success(tweets);
                        }
                    }

                    @Override
                    public void failure(TwitterException error) {
                           if(loadTweetsCallback != null) {
                            loadTweetsCallback.failure();
                        }
                    }
                }
        );
    }

    /**
     * Callback when finish loading tweets (success or failure)
     * In case of success, retrieve a list of Tweets
     */
    public abstract class LoadTweetsCallback {
        public abstract void success(List<Tweet> tweets);
        public abstract void failure();
    }
}