package fr.enssat.gazouilli;

import com.google.gson.Gson;

import fr.enssat.gazouilli.model.CommentVO;

/**
 * Format a CommentVO into the desired type
 */
public class CommentFormater {

    public static final String JSON_TYPE = "JSON";
    public static final String CSV_TYPE = "CSV";

    /**
     * Format the message source into the desired type
     * @param source
     * @param type
     * @return
     */
    public String formatMessage(CommentVO source, String type) {
        if(type.equalsIgnoreCase(JSON_TYPE)) {
            return formatPrivateMessageFromJson(source);
        } else if(type.equalsIgnoreCase(CSV_TYPE)) {
            return formatPrivateMessageFromCSV(source);
        }
        return null;
    }

    private String formatPrivateMessageFromJson(CommentVO message) {
        Gson gson = new Gson();
        return gson.toJson(message);
    }

    /**
     * TODO : à implémenter
     * @param message
     * @return
     */
    private String formatPrivateMessageFromCSV(CommentVO message) {
        return "";
    }
}

