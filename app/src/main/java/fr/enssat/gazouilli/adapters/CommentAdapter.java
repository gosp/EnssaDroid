package fr.enssat.gazouilli.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import fr.enssat.gazouilli.model.CommentVO;
import fr.enssat.gazouilli.R;

/**
 * Adapter which handle a list of private messages
 */
public class CommentAdapter extends ArrayAdapter<CommentVO> {

    // list of private messages
    List<CommentVO> commentVOList = new ArrayList<>();

    //-- Constructors

    public CommentAdapter(Context context, int resource) {
        super(context, resource);
    }

    public CommentAdapter(Context context, int resource, List<CommentVO> objects) {
        super(context, resource, objects);
        commentVOList.addAll(objects);
    }

    /**
     * provide a view for a new or old item and setup its data content
     * @param position
     * @param convertView
     * @param parent
     * @return
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        // if view is new or not in cache, inflate a new one
        if(convertView == null) {
            LayoutInflater inflater = ((Activity)parent.getContext()).getLayoutInflater();
            convertView = inflater.inflate(R.layout.private_message_item, parent, false);
        }

        // retrieve view references
        TextView pseudo = (TextView) convertView.findViewById(R.id.pseudo);
        TextView date = (TextView) convertView.findViewById(R.id.date);
        TextView message = (TextView) convertView.findViewById(R.id.message);

        // bind data to views
        CommentVO commentVO = commentVOList.get(position);
        pseudo.setText(commentVO.userPseudo);
        date.setText(commentVO.date);
        message.setText(commentVO.message);

        return convertView;
    }

    /**
     * Add a private message to the existing list
     * @param commentVO
     */
    public void addItem(CommentVO commentVO) {
        commentVOList.add(commentVO);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return commentVOList.size();
    }

    @Override
    public CommentVO getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    /**
     * refresh adapter's content
     * @param commentVOList
     */
    public void refresh(List<CommentVO> commentVOList) {
        this.commentVOList.clear();
        this.commentVOList.addAll(commentVOList);
        notifyDataSetChanged();
    }
}
