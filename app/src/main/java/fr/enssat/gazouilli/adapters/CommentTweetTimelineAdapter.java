package fr.enssat.gazouilli.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import com.twitter.sdk.android.core.models.Tweet;
import com.twitter.sdk.android.tweetui.BaseTweetView;
import com.twitter.sdk.android.tweetui.Timeline;
import com.twitter.sdk.android.tweetui.TweetTimelineListAdapter;

import java.util.List;

import fr.enssat.gazouilli.model.CommentVO;
import fr.enssat.gazouilli.views.CommentCompactTweetView;
import fr.enssat.gazouilli.storage.AppDatabaseHelper;

/**
 * Adapter which handle a list of CustomCompactTweetView
 */
public class CommentTweetTimelineAdapter extends TweetTimelineListAdapter {

    // database helper
    protected AppDatabaseHelper appDatabaseHelper;

    // Listener for more button clicks events
    protected OnMoreButtonClickListener moreButtonClickListener = null;

    /**
     * Constructor
     * @param context
     * @param timeline
     */
    public CommentTweetTimelineAdapter(Context context, Timeline<Tweet> timeline) {
        super(context, timeline);
    }

    /**
     * provide a view for an item and bind its data
     * @param position
     * @param convertView
     * @param parent
     * @return
     */
    public View getView(int position, View convertView, ViewGroup parent) {
        Object rowView = convertView;
        final Tweet tweet = this.getItem(position);
        CommentCompactTweetView commentCompactTweetView = null;
        final long id = tweet.getId();

        // if view is null (new item or older not in cache) provide a new one
        if(convertView == null) {
            CommentCompactTweetView tv = new CommentCompactTweetView(this.context, tweet, this.styleResId);
            tv.setOnActionCallback(this.actionCallback);
            rowView = tv;
        } else { // attach tweet content to view
            ((BaseTweetView)convertView).setTweet(tweet);
        }

        // get access to view item instance
        commentCompactTweetView = ((CommentCompactTweetView)rowView);

        // retrieve database helper
        if(appDatabaseHelper == null) {
            appDatabaseHelper = new AppDatabaseHelper(this.context);
        }

        // retrieve 3 last comments of this tweet
        List<CommentVO> commentVOList = appDatabaseHelper.getThreeLastPrivateMessageListFromTweet(id);

        // reset comments views state
        commentCompactTweetView.resetCommentViews();

        // if there isn't any comment, then hide comments
        if(commentVOList.size() <= 0) {
            commentCompactTweetView.tw__comment.setVisibility(View.GONE);
        } else { // display three last comments at most
            // displays first comment
            if(commentVOList.size() >= 1) {
                commentCompactTweetView.tw_comment1_message.setText(commentVOList.get(0).message);
                commentCompactTweetView.tw_comment1_pseudo.setText(commentVOList.get(0).userPseudo);
                commentCompactTweetView.tw_comment1.setVisibility(View.VISIBLE);
                commentCompactTweetView.tw__comment.setVisibility(View.VISIBLE);
            }
            // displays a second comment
            if(commentVOList.size() >= 2) {
                commentCompactTweetView.tw_comment2_message.setText(commentVOList.get(1).message);
                commentCompactTweetView.tw_comment2_pseudo.setText(commentVOList.get(1).userPseudo);
                commentCompactTweetView.tw_comment2.setVisibility(View.VISIBLE);
            }

            // displays a third comment
            if(commentVOList.size() >= 3) {
                commentCompactTweetView.tw_comment3_message.setText(commentVOList.get(2).message);
                commentCompactTweetView.tw_comment3_pseudo.setText(commentVOList.get(2).userPseudo);
                commentCompactTweetView.tw_comment3.setVisibility(View.VISIBLE);
            }
        }

        // Listen for click event on more button
        commentCompactTweetView.setMoreButtonClickListener(new CommentCompactTweetView.OnMoreButtonClickListener() {
            @Override
            public void onMoreButtonClick() {
                if (moreButtonClickListener != null) {
                    moreButtonClickListener.onTweetMoreButtonClick(id);
                }
            }
        });

        return (CommentCompactTweetView)rowView;
    }

    /**
     * Interface used to listen click events on tweets more button
     */
    public interface OnMoreButtonClickListener {
        void onTweetMoreButtonClick(Long tweetId);
    }

    /**
     * Listen to more button click events on a tweet
     */
    public void setMoreButtonClickListener(OnMoreButtonClickListener moreButtonClickListener) {
        this.moreButtonClickListener = moreButtonClickListener;
    }

    /**
     * Cannot use builder with this class !
     * Builder is made private because it calls a parent's private constructor
     * which is inacessible
     */
    private class Builder extends TweetTimelineListAdapter.Builder {

        public Builder(Context context) {
            super(context);
        }

    }
}
