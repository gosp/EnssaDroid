package fr.enssat.gazouilli.model;

/**
 * VO which represents a private message
 */
public class CommentVO {
    // database internal private message id
    public int id;

    // id the comment's author
    public String messageID;

    // nickname of the author
    public String userPseudo;

    // publish comment date
    public String date;

    // Twitter's tweet id
    public long tweetID;

    // private message content
    public String message;

    /**
     * Constructor
     * @param messageID
     * @param userPseudo
     * @param date
     * @param tweetID
     * @param message
     */
    public CommentVO(String messageID, String userPseudo, String date, long tweetID, String message) {
        this.messageID = messageID;
        this.userPseudo = userPseudo;
        this.date = date;
        this.tweetID = tweetID;
        this.message = message;
    }

    /**
     * Builder pattern for creating a private message reference
     */
    public static class Builder {
        private String messageID;
        private String userPseudo;
        private String date = "00-00-0000";
        private long tweetID;
        private String message;

        public Builder(String messageID, String userPseudo, long tweetID, String message) {
            this.messageID = messageID;
            this.userPseudo = userPseudo;
            this.tweetID = tweetID;
            this.message = message;
        }

        public CommentVO.Builder setDate(String date) {
            this.date = date;
            return this;
        }

        public CommentVO build() {
            return new CommentVO(this.messageID, this.userPseudo, this.date, this.tweetID, this.message);
        }
    }
}
