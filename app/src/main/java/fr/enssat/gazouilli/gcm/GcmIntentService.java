package fr.enssat.gazouilli.gcm;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.gson.JsonParseException;

import java.util.logging.Level;
import java.util.logging.Logger;

import fr.enssat.gazouilli.storage.AppSharedPreferences;
import fr.enssat.gazouilli.CommentFactory;
import fr.enssat.gazouilli.activities.MainActivity;
import fr.enssat.gazouilli.model.CommentVO;
import fr.enssat.gazouilli.storage.AppDatabaseHelper;
import fr.enssat.gazouilli.R;
import fr.enssat.gazouilli.activities.TweetActivity;

/**
 * Service which handle GCM events
 */
public class GcmIntentService extends IntentService {

    public GcmIntentService() {
        super("GcmIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Bundle extras = intent.getExtras();
        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);

        // get message type of the incoming intent
        String messageType = gcm.getMessageType(intent);

        if (extras != null && !extras.isEmpty()) {

            if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE.equals(messageType)) {
                Logger.getLogger("GCM_RECEIVED").log(Level.INFO, extras.toString());

                // extract message from intent
                String incomingMessage = extras.getString("message");

                CommentVO commentVO;
                try {
                    // parse incoming message
                    CommentFactory commentFactory = new CommentFactory();
                    commentVO = commentFactory.getMessage(incomingMessage, CommentFactory.JSON_TYPE);

                    // get shared preference helper
                    AppSharedPreferences appSharedPreferences = new AppSharedPreferences(getApplicationContext());

                    // if message received is unique (message id is different of previous message id) process
                    if(!appSharedPreferences.getLastMessageIdReceived().equals(commentVO.messageID)) {
                        // get database helper and add message to the database tweet
                        AppDatabaseHelper appDatabaseHelper = new AppDatabaseHelper(getApplicationContext());
                        appDatabaseHelper.addPrivateMessage(commentVO);

                        // broadcast event
                        broadcastComment();

                        // register last message
                        appSharedPreferences.putLastMessageIdReceived(commentVO.messageID);

                        // send a new notification if user author is not current user && notification enabled by app
                        if(!appSharedPreferences.getLastMessageIdSent().equals(commentVO.messageID)
                                && appSharedPreferences.isNotificationEnabled()) {
                            sendNotification(getString(R.string.new_comment), commentVO.message, commentVO.tweetID);
                        }
                    }
                } catch (JsonParseException exception) {
                    Log.d("GcmIntentService", "error parsing incoming message : "+incomingMessage);
                }
            }
        }

        // release wake lock
        GcmBroadcastReceiver.completeWakefulIntent(intent);
    }

    private void sendNotification(String title, String content,long tweetId) {
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.mipmap.ic_enssadroid)
                        .setContentTitle(title)
                        .setContentText(content);

        // Creates an explicit intent for an Activity in your app
        Intent resultIntent = new Intent(this, MainActivity.class);
        resultIntent.putExtra(TweetActivity.INTENT_EXTRA_TWEET_ID, tweetId);

        // The stack builder object will contain an artificial back stack for the
        // started Activity.
        // This ensures that navigating backward from the Activity leads out of
        // your application to the Home screen.
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);

        // Adds the back stack for the Intent (but not the Intent itself)
        stackBuilder.addParentStack(TweetActivity.class);

        // Adds the Intent that starts the Activity to the top of the stack
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        Notification notification = mBuilder.build();
        notification.flags |= Notification.FLAG_AUTO_CANCEL;

        // mId allows you to update the notification later on.
        mNotificationManager.notify(0, notification);
    }

    protected void showToast(final String message) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
            }
        });
    }

    /**
     * notify the app, a new comment arrival
     */
    protected void broadcastComment() {
        Intent intent = new Intent("new-comment");
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }
}