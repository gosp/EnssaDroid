package fr.enssat.gazouilli.gcm;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import com.example.gosp.myapplication.backend.registration.Registration;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.extensions.android.json.AndroidJsonFactory;
import com.google.api.client.googleapis.services.AbstractGoogleClientRequest;
import com.google.api.client.googleapis.services.GoogleClientRequestInitializer;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import fr.enssat.gazouilli.Constants;

/**
 * Asynchronous task which registers user's app to the gcm server
 */
public class GcmRegistrationAsyncTask extends AsyncTask<Void, Void, String> {
    private static Registration regService = null;
    private GoogleCloudMessaging gcm;
    private Context context;

    /**
     * Constructor
     * @param context
     */
    public GcmRegistrationAsyncTask(Context context) {
        this.context = context;
    }

    @Override
    protected String doInBackground(Void... params) {
        if (regService == null) {
            // registration setup (builder)
            Registration.Builder builder = new Registration.Builder(AndroidHttp.newCompatibleTransport(),
                    new AndroidJsonFactory(), null)
                    .setRootUrl(Constants.ENSSAT_SERVER_URL)
                    .setGoogleClientRequestInitializer(new GoogleClientRequestInitializer() {
                        @Override
                        public void initialize(AbstractGoogleClientRequest<?> abstractGoogleClientRequest)
                                throws IOException {
                            abstractGoogleClientRequest.setDisableGZipContent(true);
                        }
                    });
            regService = builder.build();
        }

        // process registration
        String msg = "";
        try {
            if (gcm == null) {
                gcm = GoogleCloudMessaging.getInstance(context);
            }
            String regId = gcm.register(Constants.ENSSAT_SENDER_ID);
            msg = "Device registered, registration ID=" + regId;
            regService.register(regId).execute();
        } catch (IOException ex) {
            ex.printStackTrace();
            msg = "Error: " + ex.getMessage();
        }
        return msg;
    }

    /**
     * At end of task
     * @param msg
     */
    @Override
    protected void onPostExecute(String msg) {
        if(Constants.DEBUG) {
            Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
        }
        Logger.getLogger("REGISTRATION").log(Level.INFO, msg);
    }
}
