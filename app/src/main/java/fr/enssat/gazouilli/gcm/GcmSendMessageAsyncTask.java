package fr.enssat.gazouilli.gcm;

import android.content.Context;
import android.os.AsyncTask;

import com.example.gosp.myapplication.backend.messaging.Messaging;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.extensions.android.json.AndroidJsonFactory;
import com.google.api.client.googleapis.services.AbstractGoogleClientRequest;
import com.google.api.client.googleapis.services.GoogleClientRequestInitializer;

import java.io.IOException;

import fr.enssat.gazouilli.Constants;
import fr.enssat.gazouilli.storage.AppSharedPreferences;
import fr.enssat.gazouilli.CommentFormater;
import fr.enssat.gazouilli.model.CommentVO;

/**
 * Asynchronous task which send message over gcm
 */
public class GcmSendMessageAsyncTask extends AsyncTask<CommentVO, Void, String> {
    private static Messaging msgService = null;
    private GoogleCloudMessaging gcm;
    private Context context;

    /**
     * Constructor
     * @param context
     */
    public GcmSendMessageAsyncTask(Context context) {
        this.context = context;
    }

    @Override
    protected String doInBackground(CommentVO... params) {
        if(params.length > 0) {
            CommentVO commentVO = params[0];

            String message = new CommentFormater().formatMessage(commentVO, CommentFormater.JSON_TYPE);

            Messaging.Builder builder = new Messaging.Builder(AndroidHttp.newCompatibleTransport(),
                    new AndroidJsonFactory(), null)
                    // Need setRootUrl and setGoogleClientRequestInitializer only for local testing,
                    // otherwise they can be skipped
                    .setRootUrl(Constants.ENSSAT_SERVER_URL)
                    .setGoogleClientRequestInitializer(new GoogleClientRequestInitializer() {
                        @Override
                        public void initialize(AbstractGoogleClientRequest<?> abstractGoogleClientRequest)
                                throws IOException {
                            abstractGoogleClientRequest.setDisableGZipContent(true);
                        }
                    })
                    .setApplicationName("enssadroid");
                    msgService = builder.build();
            try {
                AppSharedPreferences appSharedPreferences = new AppSharedPreferences(context);
                appSharedPreferences.putLastMessageIdSent(commentVO.messageID);
                msgService.messagingEndpoint().sendMessage(message).execute();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return "message";
    }

    /**
     * At end of task
     */
    @Override
    protected void onPostExecute(String message) {
    }
}
